﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(enstrumanadresi.Startup))]
namespace enstrumanadresi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
